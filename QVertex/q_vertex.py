# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QVertex
                                 A QGIS plugin
 автоматизация землеустройства
                              -------------------
        begin                : 2014-07-24
        git sha              : $Format:%H$
        copyright            : (C) 2014 by Филиппов Владислав
        email                : filippov70@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os.path, sys
import os
from qgis.core import *
from . import createpoints, coordcatalog
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class Vertex:
    """QGIS Plugin Implementation."""
    def __init__(self, iface):
        """Constructor.
		print os.path.abspath(os.path.dirname(__file__) + '/svgwrite'
        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)

        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'QVertex_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference

        self.html_cataloga_data = u''

        # Declare instance attributes
        self.actions = []
        #self.menu = self.tr(u'&qVertex')
        # TODO: We are going to let the user set this up in a future iteration
        #self.toolbar = self.iface.addToolBar(u'QVertex')
        #self.toolbar.setObjectName(u'QVertex')

        # Настройки http://gis-lab.info/docs/qgis/cookbook/settings.html
        #print self.plugin_dir
        self.settings = QSettings(self.plugin_dir + os.sep + 'config.ini', QSettings.IniFormat)
        self.lastDir = self.settings.value('last_dir', os.getenv('HOME'))
        # if sys.platform.startswith('win'):
        #     self.lastDir = self.settings.value('last_dir', self.plugin_dir)
        # else:
        #     self.lastDir = self.settings.value('last_dir', self.plugin_dir)

        self.pointFindPrec = float(self.settings.value('prec', self.plugin_dir))
        self.dlg = None#QVertexDialogBase(self.iface, msk_names)
        self.dlgShiftSheet = None
        # счётчик для нумерации листов чертежей
        self.sheetNumber = 0

    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        #add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""
        self.menu = QMenu()
        self.menu.setTitle("Vertex")

        self.qvertex_createPoint = QAction("Создать характерные точки", self.iface.mainWindow())
        self.qvertex_createPoint.setEnabled(True)
        #self.qvertex_createPoint.setIcon(QIcon(":/plugins/QVertex/icons/importkk.png"))
        self.menu.addAction(self.qvertex_createPoint)


        self.qvertex_createCtalog = QAction("HTML-ведомость координат", self.iface.mainWindow())
        self.qvertex_createCtalog.setEnabled(True)
        self.menu.addAction(self.qvertex_createCtalog)

        self.qvertex_createZuList = QAction("HTML-список ЗУ", self.iface.mainWindow())
        self.qvertex_createZuList.setEnabled(True)
        self.menu.addAction(self.qvertex_createZuList)

        menu_bar = self.iface.mainWindow().menuBar()
        actions = menu_bar.actions()
        lastAction = actions[len(actions) - 1]
        menu_bar.insertMenu(lastAction, self.menu)


        # QObject.connect(self.qvertex_createVertex, SIGNAL("triggered()"), self.doCreatePublicVertexes)
        self.qvertex_createPoint.triggered.connect(self.doCreatepoint)
        # QObject.connect(self.qvertex_createTurnPoint, SIGNAL("triggered()"), self.doCreateTurnPoint)
        self.qvertex_createCtalog.triggered.connect(self.doCreateCoordcatalog)
        self.qvertex_createZuList.triggered.connect(self.doCreateZuList)


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr('&qVertex'),
                action)
            self.iface.removeToolBarIcon(action)

    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            pass

    def isObjectsSelected(self):
        if self.iface.mapCanvas().currentLayer() is not None:
            if self.iface.mapCanvas().currentLayer().selectedFeatures() is not None:
                return True
            else:
                return False
        else:
            return False

  
    def doCreatepoint(self):
        if self.isObjectsSelected():
            creator = createpoints.CreatePoints(self.iface, self.pointFindPrec)
            creator.Create(1)


    def doCreateCoordcatalog(self):
        self.html_cataloga_data = u''
        ved = coordcatalog.CatalogData(self.iface, False, False, 2,
                          self.iface.mapCanvas().mapSettings().destinationCrs())
        self.html_cataloga_data = ved.vedomost
        if self.html_cataloga_data != u'':
            self.save_catalog()
        #if self.dlg_coordcatalog is None:
        # self.dlg_coordcatalog = createCoordCatalog.CreateCoordCatalog(
        #     self.iface, self.iface.mapCanvas().mapSettings().destinationCrs(), False)
        # self.dlg_coordcatalog.setWindowModality(Qt.NonModal)
        # self.dlg_coordcatalog.show()

    def doCreateZuList(self):
        list_zu_row = u'<tr><td width="55" height="12" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0cm; padding-left: 0.05cm; padding-right: 0cm"><p><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">{0}</font></font></p></td><td width="174" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0cm; padding-left: 0.05cm; padding-right: 0cm"><p><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">{1}</font></font></p></td><td width="328" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0cm; padding-left: 0.05cm; padding-right: 0cm"><p><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">{2}</span></font></font></p></td><td width="78" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding: 0cm 0.05cm"><p align="right"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">{3}</font></font></p></td></tr>'
        list_zu_start = u'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head><meta http-equiv="content-type" content="text/html; charset=utf-8"/><title></title><meta name="generator" content="LibreOffice 6.4.3.2 (Linux)"/><meta name="created" content="2020-04-23T21:52:30.705283953"/><meta name="changed" content="2020-04-23T21:53:12.084988236"/><style type="text/css">@page { size: 21cm 29.7cm; margin: 2cm }p { margin-bottom: 0.25cm; line-height: 115%; background: transparent }</style></head><body lang="ru-RU" link="#000080" vlink="#800000" dir="ltr"><p style="margin-bottom: 0cm; line-height: 100%"><br/></p><table width="653" cellpadding="2" cellspacing="0"><col width="55"/><col width="174"/><col width="328"/><col width="78"/><tr><td width="55" height="12" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0cm; padding-left: 0.05cm; padding-right: 0cm"><p lang="ru-RU" align="center">№ <font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">участка</font></font></p></td><td width="174" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0cm; padding-left: 0.05cm; padding-right: 0cm"><p lang="ru-RU" align="center"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">Способ образования</font></font></p></td><td width="328" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0cm; padding-left: 0.05cm; padding-right: 0cm"><p lang="ru-RU" align="center"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt">Вид разрешенного использования </font></font></p></td><td width="78" style="border: 1px solid #000000; padding: 0cm 0.05cm"><p lang="ru-RU" align="center"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><font size="3" style="font-size: 12pt">Площадь,</font><font size="3" style="font-size: 12pt"><span lang="en-US"></span></font><font size="3" style="font-size: 12pt">кв.м</font></font></font></p></td></tr>'
        list_zu_end = u'</table><p style="margin-bottom: 0cm; line-height: 100%"><br/></p></body></html>'
        request = QgsFeatureRequest()
        clause = QgsFeatureRequest.OrderByClause('order', ascending=True)
        orderby = QgsFeatureRequest.OrderBy([clause])
        request.setOrderBy(orderby)
        features = None
        list_zu = list_zu_start
        
        features = self.iface.mapCanvas().currentLayer().getFeatures(request)

        for feature in features:
            name = str(feature.attribute('name'))
            create = feature.attribute('create')
            util = feature.attribute('util')
            area = str(feature.attribute('area'))
            list_zu += list_zu_row.format(name, create, util, area)

        list_zu += list_zu_end
        #if list_zu != u'':
        self.save_list(list_zu)

    def save_list(self, list_zu):
        sdir = self.lastDir
        file_name, filter_string = QFileDialog.getSaveFileName(
            None, 'Save data', sdir, 'HTML-files(*.html *.HTML)')
        if file_name is not None or not file_name == '':
            ccf = open(file_name, 'w', encoding="utf8")
            ccf.write(list_zu)
            ccf.close()

    def save_catalog(self):
        sdir = self.lastDir #os.getenv('HOME')
        file_name, filter_string = QFileDialog.getSaveFileName(
            None, 'Save data', sdir, 'HTML-files(*.html *.HTML)')
        if file_name is not None or not file_name == '':
            # print file_name + ' ved path'
            # current_path = os.path.dirname(unicode(file_name))
            # self.curr_path = current_path
            # filepath = os.path.join(current_path, '.html')

            ccf = open(file_name, 'w', encoding="utf8")
            #ccf.write(self.html_cataloga_data.encode('utf8'))
            ccf.write(self.html_cataloga_data)
            ccf.close()
