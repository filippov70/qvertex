# -*- coding: utf-8 -*-
# from compiler.ast import Node
__name__ = 'coordcatalog'
__version__ = '0.2'
__author__ = 'Filippov Vladislav'

#from pydev import pydevd
#from QVertex.tools.svgwrite.drawing import Drawing
import os
from builtins import round
import math
# Библиотека в site-packages
#from svgwrite import drawing, text, path, shapes, mm
from qgis.core import *


class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y

# Измерение дирекционного угла (румба) и горизонтального проложения


class Measure():

    def __init__(self, point1, point2, is_rumb):
        self.point1 = point1
        self.point2 = point2
        self.ddx = self.point2.x - self.point1.x
        self.ddy = self.point2.y - self.point1.y
        self.ang = -1
        self.len = -1
        self.angle = ''
        self.lenght = ''
        self.rumb = ''
        self.calclenght()
        if is_rumb:
            self.calcrumb()
        else:
            self.calcangle()

    def calclenght(self):
        a = math.pow(self.ddx, 2)
        b = math.pow(self.ddy, 2)
        self.len = math.sqrt(a + b)
        self.lenght = '{0:.2f}'.format(round(self.len, 2))

    def calcangle(self):
        if self.ddx == 0:
            if self.ddy < 0:
                self.angle = '270°0\''
            else:
                self.angle = '90°0\''
        else:
            alfa = math.fabs(math.atan(self.ddy / self.ddx)
                             * (180 / math.pi))
            if (self.ddx > 0) and (self.ddy > 0):
                self.ang = alfa
                self.calcdegmin()
            elif (self.ddx < 0) and (self.ddy > 0):
                self.ang = 180 - alfa
                self.calcdegmin()
            elif (self.ddx < 0) and (self.ddy < 0):
                self.ang = 180 + alfa
                self.calcdegmin()
            elif (self.ddx > 0) and (self.ddy < 0):
                self.ang = 360 - alfa
                self.calcdegmin()
            elif (self.ddx > 0) and (self.ddy == 0):
                self.angle = '0°0\''
            elif (self.ddx < 0) and (self.ddy == 0):
                self.angle = '180°0\''

    def calcrumb(self):
        if self.ddx == 0:
            if self.ddy < 0:
                self.angle = 'З:0° 0.0\''
            else:
                self.angle = 'В:0° 0.0\''
        else:
            alfa = math.fabs(math.atan(self.ddy / self.ddx) * (180 / math.pi))
            # print(alfa)
            if (self.ddx > 0) and (self.ddy > 0):
                self.ang = alfa
                self.rumb = 'СВ:'
                self.calcdegmin()
            elif (self.ddx < 0) and (self.ddy > 0):
                self.ang = alfa
                self.rumb = 'ЮВ:'
                self.calcdegmin()
            elif (self.ddx < 0) and (self.ddy < 0):
                self.ang = alfa
                self.rumb = 'ЮЗ:'
                self.calcdegmin()
            elif (self.ddx > 0) and (self.ddy < 0):
                self.ang = alfa
                self.rumb = 'СЗ:'
                self.calcdegmin()
            elif (self.ddx > 0) and (self.ddy == 0):
                self.angle = 'С:0° 0.0\''
            elif (self.ddx < 0) and (self.ddy == 0):
                self.angle = 'Ю:0° 0.0\''

    def calcdegmin(self):
        a = int(self.ang)
        # print(a)
        minute = (self.ang - a) * 60
        if self.rumb != '':
            self.angle = self.rumb + \
                str(a) + '° ' + str('{0:.1f}'.format(minute)) + '\''
            self.rumb = ''
        else:
            self.angle = str(a) + '°' + str('{0:.1f}'.format(minute)) + '\''


class CatalogData():

    def __init__(self, iface, is_rumb, is_onlyXY, font_size, crs):
        #self.features = iface.mapCanvas().currentLayer().selectedFeatures()
        request = QgsFeatureRequest()
        clause = QgsFeatureRequest.OrderByClause('order', ascending=True)
        orderby = QgsFeatureRequest.OrderBy([clause])
        request.setOrderBy(orderby)

        if len(iface.mapCanvas().currentLayer().selectedFeatures()) > 0:
            self.features = iface.mapCanvas().currentLayer().selectedFeatures()
        else:
            self.features = iface.mapCanvas().currentLayer().getFeatures(request)

        for clayer in iface.mapCanvas().layers():
            if clayer.name() == 'Точки':
                self.pointLayer = clayer
                break
            else:
                self.pointLayer = None

        self.fontsize = 'xx-small'
        if font_size == 2:
            self.fontsize = 'small'
        elif font_size == 3:
            self.fontsize = 'medium'
        elif font_size == 4:
            self.fontsize = 'large'
        self.crs = crs
        crsSrc = QgsCoordinateReferenceSystem(
            4326, QgsCoordinateReferenceSystem.PostgisCrsId)
        crsDest = self.crs  # QgsCoordinateReferenceSystem()
       # crsDest.createFromProj4(self.crs.authid())
        # print(self.crs.authid())
        # print(crsSrc.authid())
        self.transformer = QgsCoordinateTransform(
            crsDest, crsSrc,  QgsProject.instance())
        self.zu_multi = []  # 1 (если полигон) или N конутуров мультполигона
        self.zu = []  # контуры текущего полигона
        self.vedomost = u''
        self.catalog = '<HEAD><meta http-equiv=\"Content-type\" ' \
                       'content=\"text/html;charset=UTF-8\"><style>body table { font-size: ' + self\
            .fontsize + '; font-family: Arial;} </style><HEAD/>'
        self.geodataSVG = None
        self.geodataNewSVG = None
        self.pointDef = '<!DOCTYPE HTML><HTML><HEAD><meta http-equiv=\"Content-type\" content=\"text/html;charset=UTF-8\"> \
                     <HEAD/><BODY><p>Нажмите \'Сохранить SVG\' для сохранения геоданных в файл.</p></br><p>Граница земельного участка проходит по поворотным точкам: '
        self.pointDefHole = '</br>Внутренний обход {0}: '
        self.pointDefOuterRing = 'Контур {0}: '
        self.geodata_w = 420
        self.geodata_h = 297
        self.iface = iface
        self.multi = False
        self.area = []
        self.perimeter = []
        self.names = []
        self.is_rumb = is_rumb
        # self.prepare_data()
        # # print self.list_contours[0]
        # if is_onlyXY:
        #     self.calculateOnlyXY()
        # else:
        #     self.calculate()
        # if len(self.features) == 1:
        #     self.prepare_data(self.features[0])
        #     #print self.list_contours[0]
        #     if not is_rumb:
        #         self.calculateOnlyXY(self.features[0])
        #     else:
        #         self.calculate(self.features[0])
        #     self.vedomost = self.catalog

        # elif len(self.features) > 1:
        for feature in self.features:
            self.prepare_data(feature)
            print('Start')
            if not is_rumb:
                self.calculateOnlyXY(feature)
            else:
                self.calculate(feature)

            self.vedomost += self.catalog + u'\n'
            self.catalog = u''
            #self.zu = []  # контуры текущего полигона
            self.area = []
            self.perimeter = []
            self.multi = False
            self.zu_multi = []
            self.name = u''
    #
    #
    # def isMultiPart(self, feature):
    #     if feature.attribute(u'order') != NULL:
    #         return True

    def arithmetic_round(self, x):
        a = int(x)
        b = x - a
        if (x < 0.5):
            return round(x)
        else:
            return round(x + 0.01)

    def fucked_up_round(self, number, ndigits=0):
        ndigits += 1
        n = round(number, ndigits) * (10**ndigits)
        m = n % 10
        n -= m
        if m >= 5:
            n += 10
        n /= (10**ndigits)
        return n

    def convertCoordinate(self, point):
        trpoint = self.transformer.transform(point)
        return trpoint

    def prepare_data(self, feature):
        # Создаётся на один объект
        nameidx = self.pointLayer.fields().indexFromName('name')
        
        geom = feature.geometry()
        print (str(geom.wkbType()))
        self.names.append(feature.attributes()[nameidx])
        if geom.isMultipart():
            self.multi = True
            polygons = geom.asMultiPolygon()
            for polygone in polygons:
                poly = QgsGeometry().fromPolygonXY(polygone)
                self.area.append(self.fucked_up_round(poly.area(), 0))
                self.perimeter.append(math.floor(poly.length()))
                self.parse_polygon(polygone)
        else:
            if geom.wkbType() == QgsWkbTypes.LineString:
                print ('WKBLineString')
                self.area.append(0.0)
                self.perimeter.append(math.ceil(geom.length()))
                self.parse_polyline(geom.asPolyline())

            else:
                self.area.append(self.fucked_up_round(geom.area(), 0))
                self.perimeter.append(math.floor(geom.length()))
                self.parse_polygon(geom.asPolygon())

    # полигон может содержать один внешний и от нуля до N внутренних контуров (дырок)
    def parse_polygon(self, polygon):
        zu = []
        # print polygon
        for ring in polygon:
            list_ponts = []
            for node in ring:
                # Тут происходит переход к геодезической СК
                point = node
                latlon = self.convertCoordinate(node)
                x = round(point.y(), 2)
                y = round(point.x(), 2)
                lat = self.toDMS(latlon.y())
                lon = self.toDMS(latlon.x())
                name = ""
                for pointfeature in self.pointLayer.getFeatures():
                    geom = QgsGeometry.fromPointXY(
                        node).buffer(0.1, 16)  # TODO
                    if pointfeature.geometry().intersects(geom):
                        # if pointfeature.geometry().equals(QgsGeometry.fromPoint(QgsPoint(node.x(), node.y()))):
                        name += str(pointfeature.attribute('name'))
                        break
                list_ponts.append([x, y, name, lat, lon])
                # print str(lat)
            zu.append(list_ponts)
        self.zu_multi.append(zu)

    def parse_polyline(self, polyline):
        zu = []
        print ('polyline')
        list_ponts = []
        for node in polyline:
            point = node
            x = round(point.y(), 2)
            y = round(point.x(), 2)
            name = ""
            for pointfeature in self.pointLayer.getFeatures():
                geom = QgsGeometry.fromPointXY(node).buffer(0.1, 16)
                if pointfeature.geometry().intersects(geom):
                    name += str(pointfeature.attribute('name'))
                    break
            list_ponts.append([x, y, name])

            #print (name + ";" + str(x) + ";" + str(y))
        list_ponts.append([0.0, 0.0, '-1'])
        
        zu.append(list_ponts)
        self.zu_multi.append(zu)

    def toDMS(self, coord):
        deg = int(coord)
        dminit = (coord - deg) * 60
        minit = int(dminit)
        #dsec = (coord - deg - minit / 60) * 3600
        dsec = 60 * (dminit - minit)
        sec = round(dsec, 2)
        return str(str(deg)) + '° ' + str(str(minit)) + "' " + str(str(sec)) + "''"

    def calculate(self):
        iter_contour = 0
        iter_ring = 0
        catalog_all_data = ''  # вся ведомость со всеми контурами
        if self.features[0].attributes()[self.features[0].fieldNameIndex('name')] != None:
            name = self.features[0].attributes(
            )[self.features[0].fieldNameIndex('name')]
            self.catalog += ('<h4>Условный номер земельного участка ' +
                             name + '</h4>')
        for zu in self.zu_multi:
            contour_table = ''  # ведомость одного контура
            catalog_data = ''
            catalog_header = ''
            if self.multi and len(self.zu_multi) > 1:
                contour_header = '<h3>Контур ' + \
                    str(iter_contour + 1) + '</h3>'
                contour_table += contour_header
            contour_table += '<TABLE CELLSPACING=\"0\" COLS=\"7\" BORDER=\"0\"><COLGROUP SPAN=\"0\" WIDTH=\"85\"></COLGROUP>{0}</TABLE>'
            empty = '<TD STYLE=\"border-top: 1px solid #000000; border-bottom: 1px solid #000000; ' \
                    'border-left: 1px solid #000000; border-right: 1px solid #000000\" HEIGHT=\"17\" ALIGN=\"CENTER\">{0}</TD>'
            catalog_header += empty.format('№')
            catalog_header += empty.format('X, м')
            catalog_header += empty.format('Y, м')
            catalog_header += empty.format('широта, гмс')
            catalog_header += empty.format('долгота, гмс')
            if self.is_rumb:
                catalog_header += empty.format('Румб')
            else:
                catalog_header += empty.format('Дир. угол')
            catalog_header += empty.format('Расст., м')
            catalog_data += '<TR>{0}</TR>'.format(catalog_header)

            for ring in zu:
                iter_node = 0
                for point in ring:

                    # print point
                    if (iter_node >= 0) and (iter_node < len(ring) - 1):
                        point_num = ring[iter_node][2]
                        point1 = Point(ring[iter_node][0],
                                       ring[iter_node][1])
                        point2 = Point(ring[iter_node + 1][0],
                                       ring[iter_node + 1][1])
                        measure = Measure(point1, point2, self.is_rumb)
                        catalog_data += self.decorate_value_html(
                            [str(point_num), ring[iter_node][0],
                             ring[iter_node][1], measure.angle,
                             measure.lenght, ring[iter_node][3], ring[iter_node][4]])
                        self.zu_multi[iter_contour][iter_ring][iter_node].append(
                            measure.angle)
                        self.zu_multi[iter_contour][iter_ring][iter_node].append(
                            measure.lenght)

                    elif iter_node == len(ring) - 1:
                        point1 = Point(ring[iter_node - 1]
                                       [0], ring[iter_node - 1][1])
                        point2 = Point(ring[0][0], ring[0][1])
                        measure = Measure(point1, point2, self.is_rumb)
                        self.zu_multi[iter_contour][iter_ring][iter_node].append(
                            measure.angle)
                        self.zu_multi[iter_contour][iter_ring][iter_node].append(
                            measure.lenght)
                        catalog_data += self.decorate_value_html(
                            [str(ring[0][2]), ring[0][0], ring[0][1], '', '', ring[0][3], ring[0][4]], True)
                    iter_node += 1
                iter_ring += 1
                # Отделение 'дырки'
                if len(zu) > 1:
                    if iter_ring != len(zu):
                        catalog_data += empty.format('--') + empty.format('--') +\
                            empty.format('--') + empty.format('--') + empty.format(
                                '--') + empty.format('--') + empty.format('--')
            catalog_all_data += catalog_data
            self.catalog += contour_table.format(catalog_data)
            self.catalog += '<p>Площадь: {0} кв.м Периметр: {1} м</p>'.format(int(self.area[iter_contour]), self.perimeter[
                iter_contour])
            iter_contour += 1
            iter_ring = 0
        if self.multi:
            #print [iter_contour]
            self.catalog += '<BR/><strong>Общая площадь: {0} кв.м Общий периметр: {1} м</strong>'.format(str(int(sum(self.area))),
                                                                                                         str(sum(self.perimeter)))

    def calculateOnlyXY(self, feature):
        iter_contour = 0
        iter_ring = 0
        catalog_all_data = ''  # вся ведомость со всеми контурами

        if feature.attributes()[feature.fieldNameIndex('name')] != None:
            name = feature.attributes(
            )[feature.fieldNameIndex('name')]
            self.catalog += ('<h4>Условный номер земельного участка ' + str(name) + '</h4>')

        # if self.multi:
        #     self.catalog += '<strong>Площадь земельного участка: {0} кв.м</strong>'.format(str(int(sum(self.area))))
        for zu in self.zu_multi:
            contour_table = ''  # ведомость одного контура
            catalog_data = ''
            catalog_header = ''
            if self.multi and len(self.zu_multi) > 1:
                contour_header = '<p>Контур ' + str(iter_contour + 1) + '</p>'
                contour_table += contour_header
                #contour_table += '<p>Площадь контура {0} кв.м</p>'.format(int(self.area[iter_contour]))
            else:
                pass
                #contour_table += '<p>Площадь земельного участка {0} кв.м</p>'.format(int(self.area[iter_contour]))
            contour_table += '<TABLE CELLSPACING=\"0\" COLS=\"6\" BORDER=\"0\"><COLGROUP SPAN=\"0\" WIDTH=\"85\"></COLGROUP>{0}</TABLE>'
            empty = '<TD STYLE=\"border-top: 1px solid #000000; border-bottom: 1px solid #000000; ' \
                    'border-left: 1px solid #000000; border-right: 1px solid #000000\" HEIGHT=\"17\" ALIGN=\"CENTER\">{0}</TD>'

            # http://htmlbook.ru/samhtml/tablitsy/obedinenie-yacheek
            catalog_header += '<tr><td style=\"border: 1px solid \">№ точки</td><td align=center style=\"border: 1px solid \">X</td><td align=center style=\"border: 1px solid\">Y</td></tr>'
            catalog_data += (catalog_header)

            for ring in zu:
                iter_node = 0
                for point in ring:
                    point_num = point[2]
                    # print point
                    if (iter_node >= 0) and (iter_node < len(ring) - 1):
                        point1 = Point(ring[iter_node][0],
                                       ring[iter_node][1])
                        point2 = Point(ring[iter_node + 1][0],
                                       ring[iter_node + 1][1])
                        measure = Measure(point1, point2, self.is_rumb)
                        catalog_data += self.decorate_value_html([point_num, ring[iter_node][0], ring[iter_node][1], measure.lenght, measure.angle, ring[iter_node + 1][2]], False, True)
                    elif iter_node == len(ring) - 2:
                        point1 = Point(ring[iter_node - 1]
                                       [0], ring[iter_node - 1][1])
                        point2 = Point(ring[0][0], ring[0][1])
                        measure = Measure(point1, point2, self.is_rumb)
                        catalog_data += self.decorate_value_html([str(ring[iter_node][2]), ring[iter_node][0], ring[0][1], measure.lenght, measure.angle, ring[0][2]], True, True)
                    iter_node += 1
                iter_ring += 1
                # Отделение 'дырки'
                if len(zu) > 1:
                    if iter_ring != len(zu):
                        # print 'catch!'
                        catalog_data += empty.format('') + empty.format('') + empty.format('')
            catalog_all_data += catalog_data
            self.catalog += contour_table.format(catalog_data)

            iter_contour += 1
            iter_ring = 0

    def decorate_value_html(self, value, last=False, onlyXY=False):
        row1 = '<TR>{0}</TR>'
        row2 = '<TR>{0}</TR>'
        empty = '<TD STYLE=\"border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: ' \
                '1px solid #000000; border-right: 1px solid #000000\" HEIGHT=\"17\" ALIGN=\"CENTER\">{0}</TD>'
        emptyCoord = '<TD STYLE=\"border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: ' \
            '1px solid #000000; border-right: 1px solid #000000\" HEIGHT=\"17\" ALIGN=\"CENTER\">{0}</TD>'
        # https://mkaz.com/2012/10/10/python-string-format/
        num = empty.format(value[0])
        sx = emptyCoord.format('{:.2f}'.format(value[1]))
        x = emptyCoord.format(int(math.ceil(value[1])))
        sy = emptyCoord.format('{:.2f}'.format(value[2]))
        y = emptyCoord.format(int(math.ceil(value[2])))
        # if onlyXY:
        #     pass
        # else:
        a = empty.format(value[3])
        l = empty.format(value[4])
        if onlyXY:
            nextnum = empty.format(value[5])
            #data1 = num + sx + sy + a + l + nextnum
            data1 = num + sx + sy
            return row1.format(data1)
        else:
            #data1 = num + sx + sy + empty.format('</BR>') + empty.format('</BR>')
            data1 = num + sx + sy + emptyCoord.format(value[5]) + emptyCoord.format(
                value[6]) + empty.format('</BR>') + empty.format('</BR>')
            #data2 = empty.format('</BR>') + empty.format('</BR>') + empty.format('</BR>') + a + l
            data2 = empty.format('</BR>') + empty.format('</BR>') + empty.format(
                '</BR>') + empty.format('</BR>') + empty.format('</BR>') + a + l
            if not last:
                return row1.format(data1) + row2.format(data2)
            else:
                return row1.format(data1)
