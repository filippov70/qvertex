# -*- coding: utf-8 -*-
__author__ = 'filippov'

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.core import *
from .common import *

# import pydevd
#
# pydevd.settrace('localhost', port=53801, stdoutToServer=True, stderrToServer=True)

class CreatePoints():
    def __init__(self, iface, prec):
        self.iface = iface
        self.is_new_point = True
        self.cadastreLayer = None
        self.existPointIndex = 0
        self.newPointIndex = 0
        self.cadPoints = None
        self.targetLayer = None
        self.crs = iface.mapCanvas().mapSettings().destinationCrs().authid()
        self.prec = prec

        if (self.iface.mapCanvas().currentLayer() is not None) \
                and (self.iface.mapCanvas().currentLayer().selectedFeatures() is not None):
            self.selection = self.iface.mapCanvas().currentLayer().selectedFeatures()
            self.layermap = self.iface.mapCanvas().layers()
            for layer in self.layermap:
                
                if layer.type() == QgsMapLayer.VectorLayer and layer.name() == 'Кадастр':
                    
                    self.cadastreLayer = layer
                    #self.createCadPoints()

            self.createPointLyr('Точки')
        else:
            print('selection is None and currLayer is None')

    def createPointLyr(self, lyrName):
        try:
            self.targetLayer = getLayerByName(self.iface, lyrName)

            if self.targetLayer is None:
                self.targetLayer = QgsVectorLayer("Point?crs="+self.crs, lyrName, "memory")
                QgsProject.instance().addMapLayer(self.targetLayer, True)
            res = self.targetLayer.dataProvider().addAttributes([QgsField("name", QVariant.String)])
            self.targetLayer.updateFields() 
        except Exception as err:
            print(err)       

    def createCadPoints(self):
        temp = getLayerByName(self.iface, "Кадастровые точки")
        if temp is None:
            temp = QgsVectorLayer("Point?crs="+self.crs, "Кадастровые точки", "memory")
        self.cadPoints = temp
        if self.cadPoints.featureCount() < 1:  
            temp_data = temp.dataProvider()
            temp.startEditing()
            temp_data.addAttributes( [ QgsField("name", QVariant.String)] )

            iter = self.cadastreLayer.getFeatures()
            fts = []
            for feature in iter:
                # retrieve every feature with its geometry and attributes
                # fetch geometry
                geom = feature.geometry()
                if geom.isMultipart():
                    polygons = geom.asMultiPolygon()
                    for polygone in polygons:
                        for ring in polygone:
                            for vertex in ring:
                                p = QgsPointXY(vertex.x(), vertex.y())
                                fet = QgsFeature()
                                fet.setGeometry(QgsGeometry.fromPointXY(p))
                                temp_data.addFeatures( [ fet ] )
                else:
                    pol = geom.asPolygon()
                    for ring in pol:
                        for vertex in ring:
                            p = QgsPointXY(vertex.x(), vertex.y())
                            fet = QgsFeature()
                            fet.setGeometry( QgsGeometry.fromPointXY(p))
                            temp_data.addFeatures( [ fet ] )
            # Commit changes
            temp.commitChanges()
            QgsProject.instance().addMapLayer(temp, True)
        else:
            print("Cadastre points already exist")    

    def Create(self, isCadastreCheck):
        self.isCadastreCheck = isCadastreCheck
        if isCadastreCheck == 1 and self.cadastreLayer is not None:
            self.createCadPoints()

        if self.targetLayer is None:
            self.createPointLyr('Точки')

        self.targetLayer.startEditing()
        self.getLastPointIndexes()
        countExist = self.existPointIndex
        #countNew = self.newPointIndex
        print (countExist)
        for every in self.selection:
            geom = every.geometry()
            # if geom.isMultipart():
            if geom.wkbType() == QgsWkbTypes.MultiPolygon:    
                polygons = geom.asMultiPolygon()
                for polygone in polygons:
                    for ring in polygone:
                        count = 0
                        for i in ring:
                            if count < len(ring) - 1:
                                count += 1
                                # print 'counter ' + str(count)
                                # проверка на наличие существующей точки
                                if self.checkExistPoint(i):
                                    pass
                                    #self.createPointOnLayer(i, countExist)
                                else:
                                    countExist += 1
                                    self.createPointOnLayer(i, countExist)
            if geom.wkbType() == QgsWkbTypes.Polygon:
                print('not Multipart geometry')
                rings = geom.asPolygon()
                for ring in rings:
                    count = 0
                    for i in ring:
                        if count < len(ring) - 1:
                            count += 1
                            # проверка на наличие существующей точки в кадастре и на слое Точки
                            if self.checkExistPoint(i):
                                pass
                                #self.createPointOnLayer(i, countExist)
                            else:
                                countExist += 1
                                self.createPointOnLayer(i, countExist)
            if geom.wkbType() == QgsWkbTypes.LineString:   
                print('WKBLineString')
                poly = geom.asPolyline()
                for i in poly:
                    # проверка на наличие существующей точки в кадастре и на слое Точки
                    if self.checkExistPoint(i):
                        pass
                        #self.createPointOnLayer(i, countExist)
                    else:
                        countExist += 1
                        self.createPointOnLayer(i, countExist)
        self.targetLayer.commitChanges()
        self.targetLayer.triggerRepaint()

    def createPointOnLayer(self, point, name):
        feature = QgsFeature()
        feature.initAttributes(len(self.targetLayer.dataProvider().attributeIndexes()))
        feature.setGeometry(QgsGeometry.fromPointXY(point))
        # print point.x(), point.y()
        #if isNew and self.isCadastreCheck == 1:
            # TODO 'Сделать возможность настройки'
        numvalue = str(name)
        #else:
        #    numvalue = str(name)
        feature.setAttribute(self.targetLayer.fields().indexFromName('name'), numvalue)
        # feature.setAttribute(self.targetLayer.fields().indexFromName(u'prec'), u'0.10')
        # feature.setAttribute(self.targetLayer.fields().indexFromName(u'isdel'), 0)
        # feature.setAttribute(self.targetLayer.fields().indexFromName(u'type'), 0)
        # feature.setAttribute(self.targetLayer.fields().indexFromName(u'hold'), u'Закрепление отсутствует')
        self.targetLayer.dataProvider().addFeatures([feature])

    # проверка на наличие существующих точек (вершин) на кадастровом слое и
    # на слое с ЗУ
    def checkExistPoint(self, point):
        geom = QgsGeometry.fromPointXY(point).buffer(0.05, 16)
        # if self.cadPoints is not None:
        #     # features = self.cadastreLayer.getFeatures()
        #     features = self.cadPoints.getFeatures()
        # else:
        features = self.targetLayer.getFeatures()

        for feature in features:
            # geom = QgsGeometry.fromPointXY(point).buffer(0.02, 16)
            if feature.geometry().intersects(geom):
                # if  QgsGeometry.fromPointXY(QgsPointXY(point.x(), point.y())).equals(feature.geometry()):
                return True
            # if feature.geometry().intersects(geom):
            #     return True
        return False

    def getLastPointIndexes(self):
        maxValue = 0
        valExist = 0
        #valNew = 0
        featiter = self.targetLayer.getFeatures()
        idx = self.targetLayer.fields().indexFromName('name')
        for feature in featiter:
            val = feature.attributes()[idx]
            valExist = int(val)
            # print 'val' + str(val)
            if valExist > maxValue:
                maxValue = valExist
                if maxValue == 0:
                    maxValue = 1
        self.existPointIndex = int(maxValue)
        #self.newPointIndex = int(valNew)
        #return maxValue

    # depricated
    def getLastPointName(self):
        maxValue = 0
        featiter = self.targetLayer.getFeatures()
        for feature in featiter:
            idx = self.targetLayer.fields().indexFromName('name')
            val = feature.attributes()[idx]
            if val[:1] == 'н':
                val = int(val[1:])
            else:
                val = int(val)
            # print 'val' + str(val)

            if val > maxValue:
                maxValue = val
                if maxValue == 0:
                    maxValue = 1
        # print 'get max'+str(maxValue)
        return maxValue
