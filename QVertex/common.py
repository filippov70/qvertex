# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QVertex
                                 A QGIS plugin
 автоматизация землеустройства
                              -------------------
        begin                : 2015-12-20
        git sha              : $Format:%H$
        copyright            : (C) 2014 by Филиппов Владислав
        email                : filippov70@gmail.com
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.core import *

class PointDef():
    def __init__(self, ptGeometry, name):
        self.name = name
        self.x = ptGeometry.y()
        self.y = ptGeometry.x()
        self.type = None
        self.geojson = '{"type": "Feature", "properties": {"name": "_n_"}, "geometry": {"type": "Point", "coordinates": [_x_, _y_]} }'

    def asGeoJSON(self):
        name = self.geojson.replace('_n_', self.name)
        x = round(self.x, 2)
        sx = str('{:.2f}'.format(x))
        y = round(self.y, 2)
        sy = str('{:.2f}'.format(y))
        coordx = name.replace('_x_', sx)
        coordy = coordx.replace('_y_', sy)
        return coordy


class ParcelDef():
    def __init__(self, name, contours):
        if name is None:
            name = '--'
        else:    
            self.name = name
        self.json = '{"type": "ЗУ", "name": "' + \
            name + ', contours: [' + contours + ']"}'

    def asJSON(self):
        return self.json

#============================================================
# shared members

def getLayerByName(iface, layerName):
    for layer in iface.mapCanvas().layers():
        if layer.name() == layerName:
            return layer
    return None

# x, y as GIS
def getPointAtPosition(pointLayer, x, y):
    for feature in pointLayer.getFeatures():
        if QgsGeometry.fromPoint(QgsPoint(x, y)).equals(feature.geometry()):
            idx = pointLayer.fieldNameIndex('name')
            val = feature.attributes()[idx]
            point = PointDef(feature.geometry().asPoint(), val)
            return point
            
# return points
def getPointsByGeometry (geometry, pointLayer):
    returnjson_t = '{"type": "_typename_", "data": [_datatemp_]}'
    contours_t = ''
    if geometry.wkbType() == QGis.WKBMultiPolygon:
        for polygon in geometry.asMultiPolygon():
            ringsjson_t = '['
            for ring in polygon:
                ringjson_t = '['
                for point in ring:
                    ringjson_t += getPointAtPosition(
                        pointLayer, point.x(), point.y()).asGeoJSON() + ','
                ringjson = ringjson_t[:-1]
                ringjson += ']'
                ringsjson_t += ringjson + ','
            ringsjson = ringsjson_t[:-1]
            ringsjson += ']'
            contours_t += ringsjson + ','
        contours = contours_t[:-1]
        a = returnjson_t.replace('_typename_', 'multipolygon')
        returnjson = a.replace('_datatemp_', contours)

    elif geometry.wkbType() == QGis.WKBPolygon:
        ringsjson_t = '['
        for ring in geometry.asPolygon():
            ringjson_t = '['
            for point in ring:
                ringjson_t += getPointAtPosition(
                    pointLayer, point.x(), point.y()).asGeoJSON() + ','
            ringjson = ringjson_t[:-1]
            ringjson += ']'
            ringsjson_t += ringjson + ','
        ringsjson = ringsjson_t[:-1]
        ringsjson += ']'
        a = returnjson_t.replace('_typename_', 'polygon')
        returnjson = a.replace('_datatemp_', ringsjson)

    elif geom.wkbType() == QGis.WKBLineString:
        for ring in geometry.asLineString():
            for point in ring:
                points.append(getPointAtPosition(
                    pointLayer, point.x(), point.y()))
    # elif geom.wkbType() == QGis.WKBMultiLineString:
    #     pass
    return returnjson
